def cross_product(v1, v2):
    '''Computes the cross product between two 2-dimensional vectors

    Args:
        v1 (list): The first vector
        v2 (list): The second vector

    Raises:
        RuntimeError: Invalid vector dimensions

    Returns:
        float: The cross product
    '''
    dim = len(v1)

    if dim != 2:
        raise RuntimeError("Only two-dimensional vectors supported")
    if dim != len(v2):
        raise RuntimeError("Vector dimensions do not match")

    return v1[0]*v2[1] - v1[1]*v2[0]
