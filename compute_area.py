from cross_product import cross_product


def compute_triangle_area(p1, p2, p3):
    '''Computes the area of the triangle defined by the three given points

    Args:
        p1 (list): The first triangle corner
        p2 (list): The second triangle corner
        p3 (list): The third triangle corner

    Raises:
        RuntimeError: Invalid point dimensions

    Returns:
        float: The cross product
    '''
    if any(len(p) != 2 for p in [p1, p2, p3]):
        raise RuntimeError("Only two-dimensional points supported")

    v1 = [p2[i] - p1[i] for i in range(2)]
    v2 = [p3[i] - p1[i] for i in range(2)]
    return abs(cross_product(v1, v2))*0.5
