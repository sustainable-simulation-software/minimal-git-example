This repository serves as a minimal example for a git workflow, accompanying the
material of the course on sustainable simulation software development hosted at
[gitlab.com/sustainable-simulation-software/course-material](https://gitlab.com/sustainable-simulation-software/course-material).

The commit history illustrates how the feature of computing the area of a triangle
is implemented, using an implementation of the cross product under the hood. The
cross product and the triangle area computation algorithm are developed on separate branches
and the implementations are tested individually.

Before merging, all feature branches are rebased in order to end up with
a thin commit graph, which allows for easy identification of the commits related
to a particular feature (see [here](https://gitlab.com/sustainable-simulation-software/minimal-git-example/-/network/main)).
Finally, some refactoring is done in a last merge request. 

This should serve as an illustration of the benefits of:

- small commits whose content can be easily deduced from the commit message
- thin commit graphs as a result of rebasing before merging
- commiting the tests __always__ after (or with) the feature code to ensure that the test pipeline passes on each commit
