from common import float_eq
from compute_area import compute_triangle_area


class Test_area:
    def test_triangle(self):
        area = compute_triangle_area([1.0, 0.0], [0.0, 1.0], [1.0, 1.0])
        assert float_eq(area, 0.5)
