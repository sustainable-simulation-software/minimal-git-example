def float_eq(a, b, eps=1e-6, cmp_style='absolute'):
    '''Checks two floating point values for equality

    Args:
        a (float): The first value
        b (float): The second value
        eps (float): The epsilon to use for the equality check
        cmp_style (str): Comparison style to use (absolute/relative)

    Raises:
        NotImplementedError: cmp_style not implemented

    Returns:
        bool: True if the two values are considered equal, False otherwise
    '''

    if cmp_style == 'absolute':
        return abs(a - b) <= eps
    raise NotImplementedError(
        'Chosen cmp_style="{}" not yet implemented'.format(cmp_style)
    )
