import pytest
from common import float_eq


class Test_float_eq:
    def test_equal(self):
        assert float_eq(1.0, 1.0)
        assert float_eq(1.0, 1.0 + 1e-7)
        assert float_eq(1.0, 1.0 + 1e-6, eps=1e-5)

    def test_unequal(self):
        assert not float_eq(1.0, 0.9)
        assert not float_eq(1.0, 1.0 + 1e-5)
        assert not float_eq(1.0, 1.0 + 1e-8, eps=1e-9)

    def test_not_implemented_cmp_style(self):
        with pytest.raises(NotImplementedError):
            float_eq(1.0, 1.0, cmp_style='relative')
