from common import float_eq
from cross_product import cross_product


class Test_cross_product:
    def test_orthogonal(self):
        cp = cross_product([1.0, 0.0], [0.0, 1.0])
        assert float_eq(cp, 1.0)

    def test_parallel(self):
        cp = cross_product([1.0, 0.0], [1.0, 0.0])
        assert float_eq(cp, 0.0)

    def test_diagonal(self):
        cp = cross_product([1.0, 0.0], [1.0, 1.0])
        assert float_eq(cp, 1.0)
